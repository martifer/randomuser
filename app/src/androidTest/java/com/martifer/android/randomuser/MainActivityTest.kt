package com.martifer.android.randomuser


import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.martifer.android.randomuser.ui.main.MainActivity
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    // Important this test only works for the first iteration (with no data in DB): TODO: improve for next iterations
    @Test
    fun mainActivityTest() {
        val buttonMoreResults = onView(
            allOf(
                withId(R.id.fab), withText("MORE RESULTS"),
                withParent(
                    allOf(
                        withId(R.id.mainfragment),
                        withParent(withId(R.id.fragment))
                    )
                ),
                isDisplayed()
            )
        )

        buttonMoreResults.check(matches(isDisplayed()))

        onView(isRoot()).perform(waitFor(1000))

        val imageView = onView(
            allOf(
                withId(R.id.imageUser),
                withParent(withParent(withId(R.id.main_recycler))),
                isDisplayed()
            )
        )
        imageView.check(matches(isDisplayed()))

        val button3 = onView(

            allOf(
                withId(R.id.button_delete), withText("Remove User"),
                withParent(withParent(withId(R.id.main_recycler))),
                isDisplayed()
            )
        )
        button3.check(matches(isDisplayed()))


        val textView = onView(
            allOf(
                withId(R.id.textView_name),
                withParent(withParent(IsInstanceOf.instanceOf(android.view.ViewGroup::class.java))),
                isDisplayed()
            )
        )
        textView.check(matches(isDisplayed()))

        onView(withId(R.id.main_recycler)).check(matches(hasChildCount(1)))


        // check more results button works we need a delay
        buttonMoreResults.perform(click())

        onView(isRoot()).perform(waitFor(1000))

        // So at this moment we should see 2 elements on recycler view
        onView(withId(R.id.main_recycler)).check(matches(hasChildCount(2)))
    }

    fun waitFor(delay: Long): ViewAction? {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> = isRoot()
            override fun getDescription(): String = "wait for $delay milliseconds"
            override fun perform(uiController: UiController, v: View?) {
                uiController.loopMainThreadForAtLeast(delay)
            }
        }
    }

}
