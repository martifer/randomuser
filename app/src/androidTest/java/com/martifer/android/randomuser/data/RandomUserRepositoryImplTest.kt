package com.martifer.android.randomuser.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.martifer.android.randomuser.TestUtil
import com.martifer.android.randomuser.data.local.RandomUserLocalDataSource
import com.martifer.android.randomuser.data.local.UserDao
import com.martifer.android.randomuser.data.remote.RandomUserApiClient
import com.martifer.android.randomuser.data.remote.RandomUserRemoteDataSource
//import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response
import java.net.HttpURLConnection
import java.util.Optional.empty


//https://www.paradigmadigital.com/dev/testing-android-tests-unitarios/

@RunWith(MockitoJUnitRunner::class)
class RandomUserRepositoryImplTest {

    @Mock
    private lateinit var remoteDataSource: RandomUserRemoteDataSource
    @Mock
    private lateinit var localDataSource: RandomUserLocalDataSource


    private lateinit var repository: RandomUserRepositoryImpl

    @Before
    fun setUp() {
        repository = RandomUserRepositoryImpl(
                local = localDataSource,
                remote = remoteDataSource
        )
    }


    @Test
    fun getUsers () = runBlocking {
        //whenever(repository.getUsers(4)).thenReturn(mock(<Response<ApiResponse>>))

        val response = remoteDataSource.getUsers(4)

        assertEquals(200, response.code())
    }












    /*@Test
    fun `fetch details and check response Code 200 returned`(){
        // Assign
        val response = MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(MockResponseFileReader("success_response.json").content)
        mockWebServer.enqueue(response)
        // Act
        val  actualResponse = apiHelper.getEmployeeDetails().execute()
        // Assert
        assertEquals(response.toString().contains("200"),actualResponse.code().toString().contains("200"))
    }*/


    @Test
    @Throws(Exception::class)
    fun writeUserAndReadInList() = runBlocking {
        val testUtil: TestUtil = TestUtil()
        val userEntity: UserEntity = testUtil.createUser()
        val userEntityList: List<UserEntity> = listOf(userEntity)

        localDataSource.insertAll(userEntityList)
        val byName = localDataSource.readFilteredUsers("Ferrando")
        assertEquals(byName.first().get(0).user.name.last, "Ferrando")
    }



    @Test
    fun insertAll() {
    }

    @Test
    fun readFilteredUsers() {
    }

    @Test
    fun readUserById() = runBlocking{
        val id = "1"
        val flowObject = localDataSource.readUserById(id)
        assertEquals(flowObject.first().id,id)
    }

    @Test
    fun updateHidden() = runBlocking {
        localDataSource.updateHidden(true,1)
        val flowObject = localDataSource.readUserById("1")
        assertEquals(flowObject.first().hidden,true)
    }

    @Test
    fun deleteById() {
    }
}