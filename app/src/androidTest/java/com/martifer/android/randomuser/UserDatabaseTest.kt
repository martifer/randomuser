package com.martifer.android.randomuser

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.martifer.android.randomuser.data.UserEntity
import com.martifer.android.randomuser.data.local.UserDao
import com.martifer.android.randomuser.data.local.UserDatabase
import junit.framework.TestCase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class) // Annotate with @RunWith
class UserDatabaseTest : TestCase() {

    // get reference to the LanguageDatabase and LanguageDao class
    private lateinit var db: UserDatabase
    private lateinit var userDao: UserDao

    // Override function setUp() and annotate it with @Before
    // this function will be called at first when this test class is called
    @Before
    public override fun setUp() {
        // get context -- since this is an instrumental test it requires context from the running application
        val context = ApplicationProvider.getApplicationContext<Context>()
        // initialize the db and dao variable
        db = Room.inMemoryDatabaseBuilder(context, UserDatabase::class.java).build()
        userDao = db.userDao()
    }

    // Override function closeDb() and annotate it with @After
    // this function will be called at last when this test class is called
    @After
    fun closeDb() {
        db.close()
    }

    // create a test function and annotate it with @Test
    // here we are first adding an item to the db and then checking if that item
    // is present in the db -- if the item is present then our test cases pass




    @Test
    fun testUtil(){
        val testUtil: TestUtil = TestUtil()
        val userEntity: UserEntity = testUtil.createUser()

        assertThat(userEntity.user.name.last.equals("Ferrando")).isTrue()

    }

    @Test
    fun writeUserAndRead() = runBlocking{
        val testUtil: TestUtil = TestUtil()
        val userEntity: UserEntity = testUtil.createUser()
        userEntity.id = 1
        userDao.insert(userEntity)
        val byId = userDao.readUserById(1)
        val result = byId.first()

        assertThat(result.user.equals(userEntity.user)).isTrue()
    }

    @Test
    fun writeUserAndReadByFilter() = runBlocking{
        val testUtil: TestUtil = TestUtil()
        val userEntity: UserEntity = testUtil.createUser()
        userEntity.id = 1
        userDao.insert(userEntity)

        val byFirst = userDao.readFilteredUsers("Martí")
        val result = byFirst.first().size

        val byLast = userDao.readFilteredUsers("Ferrando")
        val result2 = byLast.first().size

        val byEmail = userDao.readFilteredUsers("mymail")
        val result3 = byEmail.first().size

        // we expect 3 matches
        assertThat((result+result2+result3).equals(3)).isTrue()
    }

    @Test
    fun duplicateUserTest() = runBlocking{
        val testUtil = TestUtil()
        val userEntity: UserEntity = testUtil.createUser()
        userEntity.id = 1
        userDao.insert(userEntity)
        userDao.insert(userEntity)

        val byFirst = userDao.readFilteredUsers("Martí")
        val result = byFirst.first().size

        //we inserted the same user twice so we expect only one match, duplicated users are not allowed
        assertThat(result.equals(1)).isTrue()
    }



}