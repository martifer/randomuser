package com.martifer.android.randomuser

import com.martifer.android.randomuser.data.*

class TestUtil {

    fun createUser(): UserEntity {

        val name: Name = Name("Martí", "Ferrando")
        val picture: Picture = Picture("https://randomuser.me/api/portraits/men/63.jpg")
        val date: Date = Date("december 1983", 37)
        val street: Street = Street(192, "av madrid")
        val location: Location = Location(street,"Barcelona","Spain")
        val user: User = User("male", name, "mymail@gmail.com",picture,"699011383",date,location)

        return UserEntity(user)
    }

}