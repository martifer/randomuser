package com.martifer.android.randomuser

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.martifer.android.randomuser.data.remote.RandomUserApiClient
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.*
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(AndroidJUnit4::class) // Annotate with @RunWith
class RandomUserApiClientTest : TestCase() {

    @Test
    fun getUsersTest () = runBlocking {

        val httpLoggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        val okHttpClient = OkHttpClient.Builder().apply {
            this.addInterceptor(httpLoggingInterceptor)
        }.build()

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.randomuser.me")
            .client(okHttpClient)
            .build()

        val randomUserApiClient = retrofit.create(RandomUserApiClient::class.java)

        //val repository: RandomUserRepository = RandomUserRepository()
        //repository.getUsers(2)

        val response = randomUserApiClient.getUser(2)
        // verify the response is OK
        assertThat(response.code()).isEqualTo(200)
    }
}