package com.martifer.android.randomuser.data

data class Picture(
    val medium: String
)
