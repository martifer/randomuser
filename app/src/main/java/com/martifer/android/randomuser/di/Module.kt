package com.martifer.android.randomuser.di

import android.app.Application
import androidx.room.Room
import com.martifer.android.randomuser.data.RandomUserRepositoryImpl
import com.martifer.android.randomuser.data.local.RandomUserLocalDataSource
import com.martifer.android.randomuser.data.local.UserDao
import com.martifer.android.randomuser.data.local.UserDatabase
import com.martifer.android.randomuser.data.remote.RandomUserApiClient
import com.martifer.android.randomuser.data.remote.RandomUserRemoteDataSource
import com.martifer.android.randomuser.domain.use_cases.GetUserDetailsUseCase
import com.martifer.android.randomuser.domain.use_cases.GetUsersUseCase
import com.martifer.android.randomuser.ui.MainViewModel
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val repositoryModule = module {
    single { RandomUserRepositoryImpl(get(),get()) }
}

val remoteModule = module {

    single { provideRemoteDataSource(get()) }
    single { provideRetrofit(get()) }
    single { provideIgdbApiClient(get()) }
    factory { provideLoggingInterceptor() }
    factory { provideOkHttpClient(get()) }
}

val localModule = module {
    single { provideLocalDataSource(get())}
    single { provideDatabase(androidApplication()) }
    single { provideUserDao(get()) }
}

fun provideRemoteDataSource(randomUserApiClient: RandomUserApiClient): RandomUserRemoteDataSource = RandomUserRemoteDataSource(randomUserApiClient)

fun provideIgdbApiClient(retrofit: Retrofit): RandomUserApiClient = retrofit.create(RandomUserApiClient::class.java)

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("https://api.randomuser.me")
        .client(okHttpClient)
        .build()
}

fun provideOkHttpClient( loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient.Builder().apply {
        this.addInterceptor(loggingInterceptor)
    }.build()
}

fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    return HttpLoggingInterceptor().apply {
        this.level = HttpLoggingInterceptor.Level.BODY
    }
}

fun provideLocalDataSource(userDataBase: UserDatabase): RandomUserLocalDataSource = RandomUserLocalDataSource(userDataBase)

fun provideUserDao(database: UserDatabase): UserDao {
    return  database.userDao()
}

fun provideDatabase(application: Application): UserDatabase {
    return Room.databaseBuilder(
            application,
            UserDatabase::class.java,
            "user_database"
    )
            .build()
}

val mainViewModelModule = module {
    viewModel {
        MainViewModel(get(), get(),get())
    }
}

val dispatchersModule = module {
    factory { provideDispatchers() }

}

fun provideDispatchers(): Dispatchers = Dispatchers

val useCasesModule = module {
    factory { provideGetUsersUseCase(get()) }
    factory { provideGetUserDetailsUseCase(get()) }
}


fun provideGetUsersUseCase(repositoryImpl: RandomUserRepositoryImpl): GetUsersUseCase = GetUsersUseCase(repositoryImpl)
fun provideGetUserDetailsUseCase(repositoryImpl: RandomUserRepositoryImpl): GetUserDetailsUseCase = GetUserDetailsUseCase(repositoryImpl)