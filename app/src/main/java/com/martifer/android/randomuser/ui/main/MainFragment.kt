package com.martifer.android.randomuser.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.martifer.android.randomuser.R
import com.martifer.android.randomuser.data.User
import com.martifer.android.randomuser.data.UserEntity
import com.martifer.android.randomuser.di.provideDatabase
import com.martifer.android.randomuser.ui.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.android.ext.android.get


class MainFragment : Fragment() {
    private val viewModel: MainViewModel by viewModel()

    private lateinit var vista: View

    private lateinit var adapter: MainListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view =  inflater.inflate(R.layout.main_fragment, container, false)
        vista = view
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        filter_text.doOnTextChanged { text, start, count, after ->
            readUserFilterData(text.toString())
        }
        fab.setOnClickListener { view ->
           viewModel.getUsers(1)
        }
        attachObserver()
        setupRecycler()
        loadInitialData()
    }

    private fun getUsers() {
        viewModel.getUsers(1)
    }

    fun updateRecycler(userList: List<UserEntity>){

        val size = adapter.currentList.size
        val newSize = userList.size


        Log.v("MainFragment","updateRecycler add users, usetList size: " +  size   +newSize )
        adapter.submitList(userList) {
            if(size != 0){
                main_recycler.scrollToPosition(userList.size-1)
            }
        }
    }

    fun setupRecycler() {

        val clickListenner = ClickListener()
            { id -> viewModel.hideUser(id) }

        val itemClickListener = ItemClickListener()
            {id -> goToDetails(id)}

        adapter = MainListAdapter(clickListenner,itemClickListener)

        main_recycler.adapter = adapter
        main_recycler.layoutManager = LinearLayoutManager(context)

    }

    private fun attachObserver() {
        viewModel.allUsers("").observe(viewLifecycleOwner, Observer<List<UserEntity>> {
            it?.let { updateRecycler(it) }
        })
    }

    fun loadInitialData(){
        viewModel.loadInitialData()
    }

    private fun readUserFilterData(filter: String) {
        viewModel.allUsers(filter).observe(viewLifecycleOwner) { users ->
            users.let { updateRecycler(it) }
        }
    }

    private fun goToDetails(id: Int){
        val action = MainFragmentDirections.actionGoToDetails(id)
        vista.findNavController().navigate(action)
    }
}