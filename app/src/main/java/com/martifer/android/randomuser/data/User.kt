package com.martifer.android.randomuser.data

import androidx.room.Embedded

data class User(

    val gender: String,
    @Embedded
    val name: Name,
    val email: String,
    @Embedded
    val picture: Picture,
    val phone: String,
    @Embedded
    val registered: Date,
    @Embedded
    val location: Location,

)
