package com.martifer.android.randomuser.data.remote

import com.martifer.android.randomuser.data.ApiResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RandomUserApiClient {
     @GET("/")
     suspend fun getUser(@Query("results") results: Int): Response<ApiResponse>
}
