package com.martifer.android.randomuser.data.local

import androidx.room.*
import com.martifer.android.randomuser.data.UserEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(userEntity: UserEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(userEntitys: List<UserEntity>)

    @Delete
    fun delete(user: UserEntity)

    @Query("DELETE FROM users_table WHERE id = :userId")
    suspend fun deleteByUserId(userId: Int)

    @Query("SELECT * FROM users_table ORDER BY id ASC")
    fun readUsers(): Flow<List<UserEntity>>

    @Query("SELECT * FROM users_table WHERE (first LIKE '%' || :filter || '%' OR last LIKE '%' || :filter || '%' OR email LIKE '%' || :filter || '%') AND hidden=0")
    fun readFilteredUsers(filter: String): Flow<List<UserEntity>>

    @Query("SELECT * FROM users_table WHERE id =:id")
    fun readUserById(id: Int): Flow<UserEntity>

    @Update
    suspend fun update(user: UserEntity)

    @Update
    suspend fun updateAll(vararg users: UserEntity)

    /**
     * Updating only "hidden" field
     * By order id
     */
    @Query("UPDATE users_table SET hidden=:hidden WHERE id = :id")
    suspend fun updateHidden(hidden: Boolean, id: Int)

}