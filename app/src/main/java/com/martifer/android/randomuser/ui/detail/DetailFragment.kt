package com.martifer.android.randomuser.ui.detail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.martifer.android.randomuser.R
import com.martifer.android.randomuser.databinding.DetailFragmentBinding
import com.martifer.android.randomuser.ui.MainViewModel
import com.squareup.picasso.Picasso
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailFragment: Fragment() {

    private val viewModel: MainViewModel by viewModel()

    private lateinit var viewDataBinding: DetailFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view =  inflater.inflate(R.layout.detail_fragment, container, false)

        viewDataBinding = DetailFragmentBinding.bind(view).apply {
            viewmodel = viewModel
        }
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let{
            //Using SafeArgs
            val id = DetailFragmentArgs.fromBundle(it).idArg
            Log.v("DetailsFragment", "arguments from bundle: id: " +  id);
            getUserById(id)

            viewModel.liveDataDetail.observe(this.viewLifecycleOwner, Observer {
                val urlIgmage = viewModel.liveDataDetail.value?.user?.picture?.medium
                setImageView(view,urlIgmage!!)
            })

        }
    }

    private fun getUserById(id: Int) {
        viewModel.getUserById(id)
    }

    private fun setImageView(view: View,url: String) {
        val image: ImageView = view.findViewById(R.id.imageUserDetail)
        Picasso.get().load(url).into(image)
    }
}