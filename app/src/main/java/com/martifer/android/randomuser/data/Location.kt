package com.martifer.android.randomuser.data

import androidx.room.Embedded

data class Location(
    @Embedded
    val street: Street,
    val city: String,
    val state: String,
)