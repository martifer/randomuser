package com.martifer.android.randomuser.domain.use_cases

import com.martifer.android.randomuser.data.RandomUserRepositoryImpl

class GetUserDetailsUseCase(private val repository: RandomUserRepositoryImpl) {
    operator fun invoke(number: Int) = repository.readUserById(number)
}