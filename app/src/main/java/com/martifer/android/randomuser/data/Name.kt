package com.martifer.android.randomuser.data

data class Name(
    val first: String,
    val last: String
)
