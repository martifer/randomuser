package com.martifer.android.randomuser.ui


import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.martifer.android.randomuser.data.RandomUserRepositoryImpl
import com.martifer.android.randomuser.data.User
import com.martifer.android.randomuser.data.UserEntity
import com.martifer.android.randomuser.domain.use_cases.GetUserDetailsUseCase
import com.martifer.android.randomuser.domain.use_cases.GetUsersUseCase
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.first

class MainViewModel(private val repositoryImpl: RandomUserRepositoryImpl,
                    private val getUsersUseCase: GetUsersUseCase,
                    private val getUserDetailsUseCase: GetUserDetailsUseCase,
                    private val dispatcher: Dispatchers = Dispatchers,
) : ViewModel() {

    val liveDataDetail = MutableLiveData<UserEntity>()

    var job: Job? = null

    private val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        Log.v("MainVM", "exception" )
        onError("Exception handled: ${throwable.localizedMessage}")

    }

    fun allUsers(filter: String): LiveData<List<UserEntity>> {
        Log.v("MainViewModel", "Flow trigger again with filter:" + filter)
        return repositoryImpl.readFilteredUsers(filter).asLiveData()
    }

    fun getUsers(number: Int) {

        job = CoroutineScope(dispatcher.IO + exceptionHandler).launch {
            val response = getUsersUseCase.invoke(number)
            withContext(dispatcher.Main) {
                if (response.isSuccessful) {
                    repositoryImpl.insertAll(convertToUserEntityList(response.body()?.results!!))
                        Log.d("MainVM", "success GetUsers" + response.body()?.results)
                } else {
                    Log.d("MainVM", "error GetUsers" + response.body())
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    fun loadInitialData() {
        //llegir users de la bbdd si no n hi ha fer get users.
        CoroutineScope(dispatcher.IO + exceptionHandler).launch {
            val usersFromDB = repositoryImpl.readFilteredUsers("")
            if(usersFromDB.first().isEmpty()) {
                getUsers(2)
            }
        }
    }

    fun hideUser(id: Int) {
        job = CoroutineScope(dispatcher.IO + exceptionHandler).launch {
            repositoryImpl.updateHidden(true,id)
        }
    }

    fun getUserById(id: Int) {
        job = CoroutineScope(dispatcher.IO + exceptionHandler).launch {
            val user = getUserDetailsUseCase.invoke(id)
            withContext(Dispatchers.Main) {
                liveDataDetail.setValue(user.first())
            }
        }
    }

    private fun convertToUserEntityList(users: List<User>): List<UserEntity> {
        return users.map { UserEntity(it) }
    }

    private fun onError(message: String) {
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}