package com.martifer.android.randomuser.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class UserTypeConverter {
    val gson = Gson()

    @TypeConverter
    fun userToString(user: User): String {
        return gson.toJson(user)
    }

    @TypeConverter
    fun stringToUser(userString: String): User {
        val objectType = object : TypeToken<User>() {}.type
        return gson.fromJson(userString, objectType)
    }
}