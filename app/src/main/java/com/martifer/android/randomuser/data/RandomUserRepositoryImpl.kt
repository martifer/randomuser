package com.martifer.android.randomuser.data;

import com.martifer.android.randomuser.data.local.RandomUserLocalDataSource
import com.martifer.android.randomuser.data.remote.RandomUserRemoteDataSource
import com.martifer.android.randomuser.domain.repository.RandomUserRepository
import retrofit2.Response;
import kotlinx.coroutines.flow.Flow

class RandomUserRepositoryImpl (
        private val local: RandomUserLocalDataSource,
        private val remote: RandomUserRemoteDataSource
): RandomUserRepository {

    override suspend fun getUsers(number:Int):Response<ApiResponse>
    {
        return remote.getUsers(number)
    }

    override suspend fun insertAll(users: List<UserEntity>) {
        local.insertAll(users)
    }

    override fun readFilteredUsers(name: String): Flow<List<UserEntity>> {
        return local.readFilteredUsers(name)
    }

    override fun readUserById(id: Int): Flow<UserEntity> {
        return local.readUserById(id.toString())
    }

    override suspend fun updateHidden(hidden: Boolean, id: Int) {
        local.updateHidden(hidden, id)
    }
}