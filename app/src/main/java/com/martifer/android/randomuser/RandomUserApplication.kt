package com.martifer.android.randomuser

import android.app.Application
import com.martifer.android.randomuser.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class RandomUserApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@RandomUserApplication)
            modules(listOf(
                repositoryModule,
                localModule,
                remoteModule,
                mainViewModelModule,
                dispatchersModule,
                useCasesModule
            ))
        }
    }
}