package com.martifer.android.randomuser.domain.repository

import com.martifer.android.randomuser.data.ApiResponse
import com.martifer.android.randomuser.data.UserEntity
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface RandomUserRepository {

    suspend fun getUsers(number: Int): Response<ApiResponse>
    suspend fun insertAll(users: List<UserEntity>)
    fun readFilteredUsers(name: String): Flow<List<UserEntity>>
    fun readUserById(id: Int): Flow<UserEntity>
    suspend fun updateHidden(hidden: Boolean, id: Int)

}