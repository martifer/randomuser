package com.martifer.android.randomuser.domain.use_cases

import com.martifer.android.randomuser.data.RandomUserRepositoryImpl

class GetUsersUseCase(private val repository: RandomUserRepositoryImpl) {
    suspend operator fun invoke(number: Int) = repository.getUsers(number)
}