package com.martifer.android.randomuser.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.martifer.android.randomuser.data.UserEntity
import com.martifer.android.randomuser.data.UserTypeConverter

@Database(entities = [UserEntity::class], version = 1)
@TypeConverters(UserTypeConverter::class)
abstract class UserDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
}