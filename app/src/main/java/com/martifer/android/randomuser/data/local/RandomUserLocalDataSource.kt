package com.martifer.android.randomuser.data.local

import com.martifer.android.randomuser.data.UserEntity
import kotlinx.coroutines.flow.Flow

class RandomUserLocalDataSource(
        private val database: UserDatabase
) {

    suspend fun insertAll(userEntity: List<UserEntity>) {
        database.userDao().insertAll(userEntity)
    }

    fun readFilteredUsers(filter: String): Flow<List<UserEntity>> {
        return database.userDao().readFilteredUsers(filter)
    }

    fun readUserById(id: String): Flow<UserEntity> {
        return database.userDao().readUserById(id.toInt())
    }

    suspend fun updateHidden(hidden: Boolean, id: Int){
        database.userDao().updateHidden(hidden,id)
    }
}