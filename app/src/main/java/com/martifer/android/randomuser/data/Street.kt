package com.martifer.android.randomuser.data

data class Street(
    val number: Int,
    val name: String
)
