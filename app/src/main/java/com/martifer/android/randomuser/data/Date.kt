package com.martifer.android.randomuser.data

data class Date(
    val date: String,
    val age: Int
)

