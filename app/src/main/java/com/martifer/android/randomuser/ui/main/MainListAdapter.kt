package com.martifer.android.randomuser.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.martifer.android.randomuser.R
import com.martifer.android.randomuser.data.UserEntity
import com.squareup.picasso.Picasso

class MainListAdapter(
    private val clickListener: ClickListener,
    private val itemClickListener: ItemClickListener
) : ListAdapter<UserEntity, MainListAdapter.UserViewHolder>(USERS_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(
            current.id,
            current.user.name.first,
            current.user.name.last,
            current.user.email,
            current.user.phone,
            current.user.picture.medium,
            clickListener,
            itemClickListener
        )
    }

    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nameItemView: TextView = itemView.findViewById(R.id.textView_name)
        private val surnameItemView: TextView = itemView.findViewById(R.id.textView_surname)
        private val emailItemView: TextView = itemView.findViewById(R.id.textView_email)
        private val phoneItemView: TextView = itemView.findViewById(R.id.textView_phone)
        private val buttonDelete: Button = itemView.findViewById(R.id.button_delete)
        private val image: ImageView = itemView.findViewById(R.id.imageUser)


        fun bind(
            id: Int,
            name: String?,
            surname: String?,
            email: String?,
            phone: String?,
            urlImage: String,
            clickListener: ClickListener,
            itemClickListener: ItemClickListener,

        ) {

            nameItemView.text = name
            surnameItemView.text = surname
            emailItemView.text = email
            phoneItemView.text = phone
            buttonDelete.setOnClickListener{clickListener.onClick(id)} //setOnClickListener(clickListener.onClick(7)) //
            itemView.setOnClickListener { itemClickListener.onClick(id) }


            Picasso.get().load(urlImage).into(image)

        }

        companion object {
            fun create(parent: ViewGroup): UserViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recyclerview_item, parent, false)
                return UserViewHolder(view)
            }
        }
    }

    companion object {
        private val USERS_COMPARATOR = object : DiffUtil.ItemCallback<UserEntity>() {
            override fun areItemsTheSame(oldItem: UserEntity, newItem: UserEntity): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: UserEntity, newItem: UserEntity): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}

class ClickListener(
    val clickListener: (userId: Int) -> Unit,
) {
    fun onClick(id: Int) = clickListener(id)
}

class ItemClickListener(
    val itemClickListener: (userId: Int) -> Unit,
) {
    fun onClick(id: Int) = itemClickListener(id)
}


