package com.martifer.android.randomuser.data.remote

import com.martifer.android.randomuser.data.ApiResponse
import retrofit2.Response

class RandomUserRemoteDataSource(
    private val randomUserApiClient: RandomUserApiClient
) {
    suspend fun getUsers(numero: Int): Response<ApiResponse> {
        return randomUserApiClient.getUser(numero)
    }
}