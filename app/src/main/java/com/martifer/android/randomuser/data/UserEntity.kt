package com.martifer.android.randomuser.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users_table")
class UserEntity(
    @Embedded
        var user: User,
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    var hidden: Boolean = false
}