package com.martifer.android.randomuser.data

data class ApiResponse(
    val results: List<User>,
)